mod search_item_row;

use self::search_item_row::SearchItemRow;
use crate::add_dialog::{AddFeedWidget, GFeed, SelectFeedWidget};
use crate::app::App;
use crate::i18n::i18n;
use feedly_api::FeedlyApi;
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{
    Button, CompositeTemplate, DropDown, ListBox, NamedAction, SearchEntry, Shortcut, Stack, StringList, Widget, Window,
};
use libadwaita::{subclass::prelude::AdwWindowImpl, NavigationView, Window as AdwWindow};
use news_flash::error::NewsFlashError;
use news_flash::feed_api::FeedApiError;
use news_flash::models::{Feed, FeedID, Url};
use news_flash::ParsedUrl;
use std::cell::RefCell;
use std::rc::Rc;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/discover/dialog.blp")]
    pub struct DiscoverDialog {
        pub current_query: Rc<RefCell<Option<String>>>,

        #[template_child]
        pub navigation_view: TemplateChild<NavigationView>,
        #[template_child]
        pub add_feed_widget: TemplateChild<AddFeedWidget>,
        #[template_child]
        pub select_feed_widget: TemplateChild<SelectFeedWidget>,
        #[template_child]
        pub search_entry: TemplateChild<SearchEntry>,
        #[template_child]
        pub language_dropdown: TemplateChild<DropDown>,
        #[template_child]
        pub search_page_stack: TemplateChild<Stack>,
        #[template_child]
        pub search_result_stack: TemplateChild<Stack>,
        #[template_child]
        pub search_result_list: TemplateChild<ListBox>,
        #[template_child]
        pub news_card_button: TemplateChild<Button>,
        #[template_child]
        pub tech_card_button: TemplateChild<Button>,
        #[template_child]
        pub science_card_button: TemplateChild<Button>,
        #[template_child]
        pub culture_card_button: TemplateChild<Button>,
        #[template_child]
        pub media_card_button: TemplateChild<Button>,
        #[template_child]
        pub sports_card_button: TemplateChild<Button>,
        #[template_child]
        pub food_card_button: TemplateChild<Button>,
        #[template_child]
        pub foss_card_button: TemplateChild<Button>,
        #[template_child]
        pub close_shortcut: TemplateChild<Shortcut>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DiscoverDialog {
        const NAME: &'static str = "DiscoverDialog";
        type ParentType = AdwWindow;
        type Type = super::DiscoverDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for DiscoverDialog {}

    impl WidgetImpl for DiscoverDialog {}

    impl WindowImpl for DiscoverDialog {}

    impl AdwWindowImpl for DiscoverDialog {}
}

glib::wrapper! {
    pub struct DiscoverDialog(ObjectSubclass<imp::DiscoverDialog>)
        @extends Widget, gtk4::Window, AdwWindow;
}

impl DiscoverDialog {
    pub fn new<W: IsA<Window> + GtkWindowExt>(parent: &W) -> Self {
        let dialog = glib::Object::new::<Self>();
        dialog.set_transient_for(Some(parent));

        let imp = dialog.imp();

        dialog.setup_featured(imp.news_card_button.get(), "news");
        dialog.setup_featured(imp.tech_card_button.get(), "tech");
        dialog.setup_featured(imp.science_card_button.get(), "science");
        dialog.setup_featured(imp.culture_card_button.get(), "culture");
        dialog.setup_featured(imp.media_card_button.get(), "media");
        dialog.setup_featured(imp.sports_card_button.get(), "sports");
        dialog.setup_featured(imp.food_card_button.get(), "food");
        dialog.setup_featured(imp.foss_card_button.get(), "open source");

        imp.search_entry.connect_search_changed(clone!(
            @strong imp.current_query as current_query,
            @weak dialog => @default-panic, move |search_entry|
        {
            let imp = dialog.imp();

            let query = search_entry.text();
            let query = query.as_str();
            let locale = imp.language_dropdown
                .model()
                .and_downcast::<StringList>()
                .unwrap()
                .string(imp.language_dropdown.selected())
                .map(|id| id.as_str().to_owned());

            if query.trim() != "" {
                dialog.feedly_search(locale);
            } else {
                dialog.clear_list();
                imp.search_page_stack.set_visible_child_name("featured");
            }
        }));

        imp.language_dropdown.set_selected(0);
        imp.language_dropdown.connect_selected_notify(clone!(
            @strong imp.current_query as current_query,
            @weak dialog => @default-panic, move |language_dropdown|
        {
            let imp = dialog.imp();

            let query = imp.search_entry.text();
            let query = query.as_str();
            let locale = language_dropdown
                .model()
                .and_downcast::<StringList>()
                .unwrap()
                .string(language_dropdown.selected())
                .map(|id| id.as_str().to_owned());

            if query.trim() != "" {
                dialog.feedly_search(locale);
            } else {
                dialog.clear_list();
                imp.search_page_stack.set_visible_child_name("featured");
            }
        }));

        imp.close_shortcut.set_action(Some(NamedAction::new("window.close")));

        imp.select_feed_widget.connect_local(
            "selected",
            false,
            clone!(@weak dialog => @default-panic, move |args| {
                let g_feed = args[1].get::<GFeed>()
                    .expect("The value needs to be of type `GFeed`.");
                let feed: Feed = g_feed.into();

                let imp = dialog.imp();
                imp.add_feed_widget.reset();
                imp.add_feed_widget.fill(feed);
                imp.navigation_view.push_by_tag("feed_add_page");
                None
            }),
        );

        imp.add_feed_widget.connect_local(
            "feed-added",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                dialog.close();
                None
            }),
        );

        dialog
    }

    fn feedly_search(&self, locale: Option<String>) {
        let imp = self.imp();

        let query = imp.search_entry.text().as_str().to_owned();
        let query_clone = query.clone();
        imp.current_query.replace(Some(query.clone()));
        imp.search_page_stack.set_visible_child_name("search");
        imp.search_result_stack.set_visible_child_name("spinner");
        self.clear_list();
        let count = Some(30);

        let dialog = self.clone();
        App::default().execute_with_callback(
            move |_news_flash, client| async move {
                FeedlyApi::search_feedly_cloud(&client, &query, count, locale.as_deref()).await
            },
            move |_app, search_result| {
                let imp = dialog.imp();

                if Some(query_clone) == *imp.current_query.borrow() {
                    match search_result {
                        Ok(search_result) => {
                            dialog.clear_list();

                            let result_count = search_result.results.len();
                            for search_item in search_result.results.iter() {
                                if search_item.title.is_none() {
                                    // dont show items without title
                                    continue;
                                }
                                let search_item_row = SearchItemRow::new(search_item);
                                search_item_row.connect_local(
                                    "add-feed",
                                    false,
                                    clone!(@weak dialog => @default-panic, move |args| {
                                        let url = args[1].get::<String>().expect("The value needs to be of type `String`.");
                                        dialog.parse_feed(&url);
                                        None
                                    }),
                                );
                                imp.search_result_list.insert(&search_item_row, -1);
                            }

                            if result_count > 0 {
                                imp.search_result_stack.set_visible_child_name("list");
                            } else {
                                imp.search_result_stack.set_visible_child_name("empty");
                            }
                        }
                        Err(e) => {
                            imp.search_result_stack.set_visible_child_name("list");
                            log::error!("Feedly search query failed: '{}'", e);
                        }
                    }
                    imp.current_query.take();
                }
            },
        );
    }

    fn clear_list(&self) {
        let imp = self.imp();

        while let Some(row) = imp.search_result_list.first_child() {
            imp.search_result_list.remove(&row);
        }
    }

    fn setup_featured(&self, button: Button, topic_name: &str) {
        let imp = self.imp();

        let topic_name_string = topic_name.to_owned();
        let search_entry = imp.search_entry.get();
        button.connect_clicked(move |_button| {
            search_entry.set_text(&format!("#{}", topic_name_string));
        });
    }

    fn parse_feed(&self, url: &str) {
        let imp = self.imp();
        imp.navigation_view.replace_with_tags(&["spinner"]);
        let feed_id = FeedID::new(url);
        let url = Url::parse(url).unwrap();
        let url_clone = url.clone();
        log::info!("url {url}");

        App::default().execute_with_callback(
            |_news_flash, client| async move {
                news_flash::feed_parser::download_and_parse_feed(&url_clone, &feed_id, None, &client).await
            },
            clone!(@weak self as dialog, @strong url => @default-panic, move |app, res| {
                let imp = dialog.imp();

                match res {
                    Ok(ParsedUrl::SingleFeed(feed)) => {
                        imp.add_feed_widget.reset();
                        imp.add_feed_widget.fill(*feed);
                        imp.navigation_view.replace_with_tags(&["discover", "add_feed"]);
                    },
                    Ok(ParsedUrl::MultipleFeeds(feeds)) => {
                        imp.select_feed_widget.fill(feeds);
                        imp.navigation_view.replace_with_tags(&["discover", "select_feed"]);
                    }
                    Err(error) => {
                        log::error!("No feed found for url '{}': {}", url, error);
                        dialog.close();
                        app.in_app_error(&i18n("Failed to parse feed"), NewsFlashError::API(FeedApiError::ParseFeed(error)));
                    }
                }
            }),
        );
    }
}
