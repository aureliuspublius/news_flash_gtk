use crate::i18n::i18n;
use crate::{account_widget::AccountWidget, app::App};
use gio::{Menu, MenuItem};
use glib::IsA;
use gtk4::{prelude::*, PopoverMenu, Widget};
use news_flash::models::VectorIcon;

#[derive(Clone, Debug)]
pub struct AccountPopover {
    pub widget: PopoverMenu,
    account_widget: AccountWidget,
}

impl AccountPopover {
    pub fn new<W: IsA<Widget>>(parent: &W) -> Self {
        let widget_item = MenuItem::new(Some("account_widget"), None);
        widget_item.set_attribute_value("custom", Some(&"account-box".to_variant()));

        let model = Menu::new();
        model.append_item(&widget_item);

        model.append(Some(&i18n("Logout")), Some("win.reset-account"));
        let popover = PopoverMenu::from_model(Some(&model));

        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.id().await
                } else {
                    None
                }
            },
            move |_app, res| {
                if res.map(|id| id.as_str() != "local_rss").unwrap_or(false) {
                    model.insert(1, Some(&i18n("Edit Account")), Some("win.update-login"));
                }
            },
        );

        let account_widget = AccountWidget::new();

        popover.set_parent(parent);
        popover.add_child(&account_widget, "account-box");

        AccountPopover {
            widget: popover,
            account_widget,
        }
    }

    pub fn set_account(&self, vector_icon: Option<VectorIcon>, user_name: &str) {
        self.account_widget.set_account(vector_icon, user_name);
    }
}
