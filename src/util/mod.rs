pub mod constants;
mod date_util;
mod desktop_settings;
mod error;
mod gtk_util;
mod stopwatch;

pub use date_util::DateUtil;
pub use desktop_settings::{ClockFormat, DesktopSettings};
pub use error::UtilError;
pub use gtk_util::GtkUtil;
pub use gtk_util::GTK_RESOURCE_FILE_ERROR;
use news_flash::models::CategoryMapping;
use news_flash::models::NEWSFLASH_TOPLEVEL;
use reqwest::{Client, ClientBuilder, Proxy};
pub use stopwatch::StopWatch;

use crate::settings::{ProxyModel, ProxyProtocoll, Settings};
use gio::{traits::ProxyResolverExt, Cancellable, ProxyResolver};
use news_flash::models::{CategoryID, Feed, FeedID, FeedMapping};
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::future::Future;
use std::rc::Rc;
use std::time::Duration;

pub const CHANNEL_ERROR: &str = "Error sending message via glib channel";

pub struct Util;

impl Util {
    #[cfg(allow_dead_code)]
    pub fn serialize_and_save<T: serde::Serialize>(object: &T, path: &str) -> Result<String, UtilError> {
        let data = serde_json::to_string_pretty(object).context(UtilErrorKind::Serde)?;
        fs::write(path, &data).context(UtilErrorKind::WriteFile)?;
        Ok(data)
    }

    pub fn glib_spawn_future<F: Future<Output = ()> + 'static>(future: F) {
        glib::MainContext::default().spawn_local(future);
    }

    pub fn some_or_default<T>(option: Option<T>, default: T) -> T {
        match option {
            Some(value) => value,
            None => default,
        }
    }

    pub fn ease_out_cubic(p: f64) -> f64 {
        let p = p - 1.0;
        p * p * p + 1.0
    }

    pub fn calculate_item_count_for_category(
        category_id: &CategoryID,
        feed_mappings: &[FeedMapping],
        category_mappings: &[CategoryMapping],
        item_count_map: &HashMap<FeedID, i64>,
        pending_deleted_feeds: &HashSet<FeedID>,
        pending_deleted_categories: &HashSet<CategoryID>,
    ) -> i64 {
        let mut count = 0;

        count += feed_mappings
            .iter()
            .filter_map(|m| {
                if &m.category_id == category_id {
                    if pending_deleted_feeds.contains(&m.feed_id) {
                        return None;
                    }

                    item_count_map.get(&m.feed_id)
                } else {
                    None
                }
            })
            .sum::<i64>();

        count += category_mappings
            .iter()
            .filter_map(|m| {
                if &m.parent_id == category_id {
                    if pending_deleted_categories.contains(&m.category_id) {
                        return None;
                    }

                    Some(Self::calculate_item_count_for_category(
                        &m.category_id,
                        feed_mappings,
                        category_mappings,
                        item_count_map,
                        pending_deleted_feeds,
                        pending_deleted_categories,
                    ))
                } else {
                    None
                }
            })
            .sum::<i64>();

        count
    }

    pub fn create_mappings_for_uncategorized_feeds(feeds: &[Feed], mappings: &[FeedMapping]) -> Vec<FeedMapping> {
        let mut uncategorized_mappings = Vec::new();
        for (i, feed) in feeds.iter().enumerate() {
            if !mappings.iter().any(|m| m.feed_id == feed.feed_id) {
                uncategorized_mappings.push(FeedMapping {
                    feed_id: feed.feed_id.clone(),
                    category_id: NEWSFLASH_TOPLEVEL.clone(),
                    sort_index: Some(i as i32),
                });
            }
        }

        uncategorized_mappings
    }

    pub fn discover_gnome_proxy() -> Vec<ProxyModel> {
        let mut proxy_vec = Vec::new();

        let proxy_resolver = ProxyResolver::default();
        let cancellable: Option<&Cancellable> = None;
        if let Ok(proxy_list) = proxy_resolver.lookup("http://example.com/", cancellable) {
            for http_proxy in proxy_list {
                if http_proxy != "direct://" {
                    let url = http_proxy.as_str().to_owned();
                    log::info!("HTTP proxy: '{}'", url);
                    proxy_vec.push(ProxyModel {
                        protocoll: ProxyProtocoll::Http,
                        url,
                        user: None,
                        password: None,
                    });
                }
            }
        }

        if let Ok(proxy_list) = proxy_resolver.lookup("https://example.com/", cancellable) {
            for https_proxy in proxy_list {
                if https_proxy != "direct://" {
                    let url = https_proxy.as_str().to_owned();
                    log::info!("HTTPS proxy: '{}'", url);
                    proxy_vec.push(ProxyModel {
                        protocoll: ProxyProtocoll::Https,
                        url,
                        user: None,
                        password: None,
                    });
                }
            }
        }

        proxy_vec
    }

    pub fn symbolic_icon_set_color(data: &[u8], hex_color: &str) -> Result<Vec<u8>, UtilError> {
        let svg_string = std::str::from_utf8(data).map_err(|_| UtilError::Svg)?;
        let colored_svg_string = svg_string.replace("fill:#bebebe", &format!("fill:{}", hex_color));
        Ok(colored_svg_string.as_bytes().into())
    }

    pub fn format_data_size(bytes: u64) -> String {
        let step = 1024.0;
        let kbytes = bytes as f64 / step;
        let mbytes = kbytes / step;
        let gbytes = mbytes / step;

        if bytes < 1024 {
            format!("{} bytes", bytes)
        } else if kbytes < step {
            format!("{:.1} kb", kbytes)
        } else if mbytes < step {
            format!("{:.1} mb", mbytes)
        } else {
            format!("{:.1} gb", gbytes)
        }
    }

    pub fn build_client(settings: Rc<RefCell<Settings>>) -> Client {
        let mut builder = ClientBuilder::new()
            .user_agent("Mozilla/5.0 (X11; Linux x86_64; rv:109.0)")
            .use_native_tls()
            .trust_dns(true)
            .gzip(true)
            .brotli(true)
            .timeout(Duration::from_secs(60))
            .danger_accept_invalid_certs(settings.borrow().get_accept_invalid_certs())
            .danger_accept_invalid_hostnames(settings.borrow().get_accept_invalid_hostnames());

        let mut proxys = settings.borrow().get_proxy();
        proxys.append(&mut Util::discover_gnome_proxy());

        for proxy_model in proxys {
            if proxy_model.url.starts_with("socks4") {
                continue;
            }

            let mut proxy = match &proxy_model.protocoll {
                ProxyProtocoll::All => Proxy::all(&proxy_model.url),
                ProxyProtocoll::Http => Proxy::http(&proxy_model.url),
                ProxyProtocoll::Https => Proxy::https(&proxy_model.url),
            }
            .unwrap_or_else(|_| panic!("Failed to build proxy: {}", proxy_model.url));

            if let Some(proxy_user) = &proxy_model.user {
                if let Some(proxy_password) = &proxy_model.password {
                    proxy = proxy.basic_auth(proxy_user, proxy_password);
                }
            }

            builder = builder.proxy(proxy);
        }

        builder.build().expect("Failed to build reqwest client")
    }
}
