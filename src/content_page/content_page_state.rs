use crate::sidebar::models::SidebarSelection;
use crate::{content_page::ArticleListMode, settings::SettingsError};
use news_flash::models::ArticleID;
use serde::{Deserialize, Serialize};

static JSON_NAME: &str = "content_page_state.json";

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct ContentPageState {
    sidebar_selection: SidebarSelection,
    article_list_mode: ArticleListMode,
    search_term: Option<String>,
    prefer_scraped_content: bool,
    visible_article_id: Option<ArticleID>,
    window_size: (i32, i32),
    maximized: bool,
    article_view_visible: Option<bool>,
    article_view_zoom: Option<f64>,

    #[serde(skip)]
    offline: bool,
    #[serde(skip)]
    was_online: bool,
    #[serde(skip)]
    ongoing_article_scrap: bool,
}

const ARTICLE_LIST_PAGE_SIZE: i64 = 20;

impl ContentPageState {
    pub fn load_window_size() -> Result<(i32, i32), SettingsError> {
        let path = crate::app::CONFIG_DIR.join(JSON_NAME);
        let data = std::fs::read_to_string(path)?;
        let state = serde_json::from_str::<Self>(&data)?;
        Ok(state.window_size)
    }

    pub fn load_from_file(&mut self) -> Result<(), SettingsError> {
        let path = crate::app::CONFIG_DIR.join(JSON_NAME);
        if path.as_path().exists() {
            let data = std::fs::read_to_string(&path)?;
            let state = serde_json::from_str::<Self>(&data)?;

            self.sidebar_selection = state.sidebar_selection;
            self.article_list_mode = state.article_list_mode;
            self.search_term = state.search_term;
            self.prefer_scraped_content = state.prefer_scraped_content;
            self.visible_article_id = state.visible_article_id;
            self.window_size = state.window_size;
            self.maximized = state.maximized;
            self.article_view_visible = state.article_view_visible;
            self.article_view_zoom = state.article_view_zoom;
        }

        Ok(())
    }

    pub fn write(&self) -> Result<(), SettingsError> {
        let path = crate::app::CONFIG_DIR.join(JSON_NAME);
        let data = serde_json::to_string_pretty(self)?;
        std::fs::write(path, data)?;
        Ok(())
    }

    pub fn set_window_size(&mut self, size: (i32, i32)) {
        self.window_size = size;
    }

    pub fn get_window_size(&self) -> (i32, i32) {
        self.window_size
    }

    pub fn set_maximized(&mut self, maximized: bool) {
        self.maximized = maximized;
    }

    pub fn get_maximized(&self) -> bool {
        self.maximized
    }

    pub fn page_size() -> i64 {
        ARTICLE_LIST_PAGE_SIZE
    }

    pub fn get_sidebar_selection(&self) -> &SidebarSelection {
        &self.sidebar_selection
    }

    pub fn set_sidebar_selection(&mut self, selection: SidebarSelection) {
        self.sidebar_selection = selection;
    }

    pub fn get_article_list_mode(&self) -> &ArticleListMode {
        &self.article_list_mode
    }

    pub fn set_article_list_mode(&mut self, header: ArticleListMode) {
        self.article_list_mode = header;
    }

    pub fn get_search_term(&self) -> Option<&str> {
        self.search_term.as_deref()
    }

    pub fn set_search_term(&mut self, search_term: Option<String>) {
        self.search_term = search_term;
    }

    pub fn set_offline(&mut self, offline: bool) {
        self.offline = offline;

        if !offline {
            self.was_online = true;
        }
    }

    pub fn get_offline(&self) -> bool {
        self.offline
    }

    pub fn was_online(&self) -> bool {
        self.was_online
    }

    pub fn set_visible_article_id(&mut self, id: Option<&ArticleID>) {
        self.visible_article_id = id.cloned();
    }

    pub fn get_visible_article_id(&self) -> Option<ArticleID> {
        self.visible_article_id.clone()
    }

    pub fn set_prefer_scraped_content(&mut self, pref: bool) {
        self.prefer_scraped_content = pref;
    }

    pub fn get_prefer_scraped_content(&self) -> bool {
        self.prefer_scraped_content
    }

    pub fn started_scraping_article(&mut self) {
        self.ongoing_article_scrap = true;
    }

    pub fn finished_scraping_article(&mut self) {
        self.ongoing_article_scrap = false;
    }

    pub fn is_article_scrap_ongoing(&self) -> bool {
        self.ongoing_article_scrap
    }

    pub fn get_article_view_visible(&mut self) -> Option<bool> {
        self.article_view_visible.take()
    }

    pub fn set_article_view_visible(&mut self, visible: bool) {
        self.article_view_visible.replace(visible);
    }

    pub fn get_article_view_zoom(&mut self) -> Option<f64> {
        self.article_view_zoom.take()
    }

    pub fn set_article_view_zoom(&mut self, zoom: f64) {
        self.article_view_zoom.replace(zoom);
    }
}

impl PartialEq for ContentPageState {
    fn eq(&self, other: &ContentPageState) -> bool {
        if self.sidebar_selection != other.sidebar_selection {
            return false;
        }
        if self.article_list_mode != other.article_list_mode {
            return false;
        }
        match &self.search_term {
            Some(self_search_term) => match &other.search_term {
                Some(other_search_term) => {
                    if self_search_term != other_search_term {
                        return false;
                    }
                }
                None => return false,
            },
            None => match &other.search_term {
                Some(_) => return false,
                None => {}
            },
        }
        true
    }
}
