mod article_list_column;
mod article_list_mode;
mod article_view_column;
mod content_page_state;
mod sidebar_column;

pub use self::article_list_column::ArticleListColumn;
pub use self::article_list_mode::ArticleListMode;
pub use self::article_view_column::ArticleViewColumn;
pub use self::content_page_state::ContentPageState;
pub use self::sidebar_column::SidebarColumn;

use crate::app::App;
use crate::article_list::ArticleListModel;
use crate::error::NewsFlashGtkError;
use crate::i18n::{i18n, i18n_f};
use crate::responsive::ResponsiveLayout;
use crate::settings::Settings;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::{FeedListItemID, FeedListTree, TagListModel};
use crate::undo_action::UndoDelete;
use crate::util::Util;
use chrono::{DateTime, Duration, TimeZone, Utc};
use eyre::{Result, WrapErr};
use glib::{clone, subclass};
use gtk4::{subclass::prelude::*, CompositeTemplate};
use libadwaita::{NavigationSplitView, OverlaySplitView, Toast, ToastOverlay};
use log::warn;
use news_flash::error::NewsFlashError;
use news_flash::models::{
    ArticleFilter, ArticleOrder, CategoryID, FeedID, Marked, PluginCapabilities, PluginID, Read, TagID,
    NEWSFLASH_TOPLEVEL,
};
use std::cell::RefCell;
use std::collections::HashSet;
use std::rc::Rc;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/content_page.blp")]
    pub struct ContentPage {
        #[template_child]
        pub content_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        pub outer: TemplateChild<OverlaySplitView>,
        #[template_child]
        pub inner: TemplateChild<NavigationSplitView>,
        #[template_child]
        pub article_list_column: TemplateChild<ArticleListColumn>,
        #[template_child]
        pub sidebar_column: TemplateChild<SidebarColumn>,
        #[template_child]
        pub articleview_column: TemplateChild<ArticleViewColumn>,

        pub prev_settings: RefCell<Option<Settings>>,
        pub state: Rc<RefCell<ContentPageState>>,
        pub prev_state: Rc<RefCell<ContentPageState>>,

        pub toasts: Rc<RefCell<HashSet<Toast>>>,
        pub current_undo_action: Rc<RefCell<Option<UndoDelete>>>,
        pub processing_undo_actions: Rc<RefCell<HashSet<UndoDelete>>>,
        pub responsive_layout: Rc<RefCell<Option<ResponsiveLayout>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ContentPage {
        const NAME: &'static str = "ContentPage";
        type ParentType = gtk4::Box;
        type Type = super::ContentPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ContentPage {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for ContentPage {}

    impl BoxImpl for ContentPage {}
}

glib::wrapper! {
    pub struct ContentPage(ObjectSubclass<imp::ContentPage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for ContentPage {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ContentPage {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        if let Err(error) = imp.state.borrow_mut().load_from_file() {
            log::error!("Failed to deserialize the content page state: {error}");
        }

        imp.responsive_layout.borrow_mut().replace(ResponsiveLayout::new(self));
        imp.prev_settings
            .replace(Some(App::default().settings().borrow().clone()));
    }

    pub fn state(&self) -> Rc<RefCell<ContentPageState>> {
        self.imp().state.clone()
    }

    pub fn prev_state(&self) -> Rc<RefCell<ContentPageState>> {
        self.imp().prev_state.clone()
    }

    pub fn simple_message(&self, message: &str) {
        let imp = self.imp();
        let toast = Toast::new(message);
        toast.connect_dismissed(clone!(@weak self as this => @default-panic, move |toast| {
            let imp = this.imp();
            if let Ok(mut toasts_guard) = imp.toasts.try_borrow_mut() {
                toasts_guard.remove(toast);
            };
        }));
        imp.content_overlay.add_toast(toast.clone());
        imp.toasts.borrow_mut().insert(toast);
    }

    pub fn newsflash_error(&self, message: &str, error: NewsFlashError) {
        let imp = self.imp();
        let toast = Toast::new(message);
        toast.set_button_label(Some(&i18n("details")));
        App::default().set_newsflash_error(NewsFlashGtkError::NewsFlash {
            source: error,
            context: message.into(),
        });
        toast.set_action_name(Some("win.show-error-dialog"));
        toast.connect_dismissed(clone!(@weak self as this => @default-panic, move |toast| {
            let imp = this.imp();
            if let Ok(mut toasts_guard) = imp.toasts.try_borrow_mut() {
                toasts_guard.remove(toast);
            };
        }));
        imp.content_overlay.add_toast(toast.clone());
        imp.toasts.borrow_mut().insert(toast);
    }

    pub fn dismiss_notifications(&self) {
        let imp = self.imp();

        for toast in imp.toasts.borrow_mut().drain() {
            toast.dismiss();
        }
    }

    pub fn remove_current_undo_action(&self) {
        let imp = self.imp();
        let _ = imp.current_undo_action.take();

        // update lists
        App::default().update_sidebar();
        App::default().update_article_list();
    }

    pub fn add_undo_notification(&self, action: UndoDelete) {
        let imp = self.imp();

        if let Some(current_action) = imp.current_undo_action.take() {
            log::debug!("remove current action: {}", current_action);
            Self::execute_action(&current_action, &imp.processing_undo_actions);

            // dismiss all other toasts
            for toast in imp.toasts.borrow_mut().drain() {
                toast.dismiss();
            }
        }

        let message = match &action {
            UndoDelete::Category(_id, label) => i18n_f("Deleted Category '{}'", &[label]),
            UndoDelete::Feed(_id, label) => i18n_f("Deleted Feed '{}'", &[label]),
            UndoDelete::Tag(_id, label) => i18n_f("Deleted Tag '{}'", &[label]),
        };

        let toast = Toast::new(&message);
        toast.set_button_label(Some(&i18n("undo")));
        toast.set_action_name(Some("win.remove-undo-action"));
        toast.connect_dismissed(clone!(
            @strong action,
            @weak self as this => @default-panic, move |toast| {
                let imp = this.imp();

                if let Some(current_action) = imp.current_undo_action.take() {
                    log::debug!("remove current action: {}", current_action);
                    Self::execute_action(&current_action, &imp.processing_undo_actions);
                };

                if let Ok(mut toasts_guard) = imp.toasts.try_borrow_mut() {
                    toasts_guard.remove(toast);
                };
        }));
        imp.content_overlay.add_toast(toast.clone());
        imp.toasts.borrow_mut().insert(toast);

        imp.current_undo_action.replace(Some(action));

        // update lists
        App::default().update_sidebar();
        App::default().update_article_list();
    }

    pub fn execute_pending_undoable_action(&self) {
        let imp = self.imp();

        if let Some(current_action) = self.get_current_undo_action() {
            Self::execute_action(&current_action, &imp.processing_undo_actions);
        }
    }

    pub fn get_current_undo_action(&self) -> Option<UndoDelete> {
        self.imp().current_undo_action.borrow().clone()
    }

    pub fn processing_undo_actions(&self) -> Rc<RefCell<HashSet<UndoDelete>>> {
        self.imp().processing_undo_actions.clone()
    }

    fn execute_action(action: &UndoDelete, processing_actions: &Rc<RefCell<HashSet<UndoDelete>>>) {
        processing_actions.borrow_mut().insert(action.clone());
        let callback = Box::new(
            clone!(@strong processing_actions, @strong action => @default-panic, move || {
                processing_actions.borrow_mut().remove(&action);
            }),
        );
        match action {
            UndoDelete::Feed(feed_id, _label) => {
                App::default().delete_feed(feed_id.clone(), callback);
            }
            UndoDelete::Category(category_id, _label) => {
                App::default().delete_category(category_id.clone(), callback);
            }
            UndoDelete::Tag(tag_id, _label) => {
                App::default().delete_tag(tag_id.clone(), callback);
            }
        }
    }

    pub fn sidebar_column(&self) -> &SidebarColumn {
        let imp = self.imp();
        &imp.sidebar_column
    }

    pub fn article_list_column(&self) -> &ArticleListColumn {
        let imp = self.imp();
        &imp.article_list_column
    }

    pub fn articleview_column(&self) -> &ArticleViewColumn {
        let imp = self.imp();
        &imp.articleview_column
    }

    pub fn outer(&self) -> &OverlaySplitView {
        let imp = self.imp();
        &imp.outer
    }

    pub fn inner(&self) -> &NavigationSplitView {
        let imp = self.imp();
        &imp.inner
    }

    pub fn responsive_layout(&self) -> ResponsiveLayout {
        let imp = self.imp();
        imp.responsive_layout
            .borrow()
            .clone()
            .expect("ContentPage not initialized")
    }

    pub fn load_branding(&self) {
        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    (news_flash.id().await, news_flash.user_name().await)
                } else {
                    (None, None)
                }
            },
            |app, (id, user)| {
                if let Some(id) = id {
                    if app
                        .main_window()
                        .content_page()
                        .load_branding_impl(&id, user.as_deref())
                    {
                        // try to fill content page with data
                        app.main_window().content_page().update_sidebar();
                        app.main_window().content_page().update_article_list();
                    }
                } else {
                    // in case of failure show 'welcome page'
                    app.main_window().show_welcome_page();
                }
            },
        );
    }

    fn load_branding_impl(&self, id: &PluginID, user_name: Option<&str>) -> bool {
        if self.sidebar_column().set_account(id, user_name).is_err() {
            App::default().in_app_notifiaction(&i18n("Failed to set service logo"));
            false
        } else {
            true
        }
    }

    pub fn clear(&self) {
        let imp = self.imp();
        let settings = App::default().settings();
        self.articleview_column().article_view().close_article();
        self.state().borrow_mut().set_prefer_scraped_content(false);

        let empty_list_model = ArticleListModel::new(&settings.borrow().get_article_list_order());
        self.article_list_column().article_list().update(
            empty_list_model,
            &self.state(),
            true,
            &self.prev_state().borrow().clone(),
        );

        let feed_tree_model = FeedListTree::new();
        self.sidebar_column().sidebar().update_feedlist(feed_tree_model);

        let tag_list_model = TagListModel::new();
        self.sidebar_column().sidebar().update_taglist(tag_list_model);
        self.sidebar_column().sidebar().hide_taglist();
        imp.prev_settings.replace(Some(settings.borrow().clone()));
        self.prev_state().replace(self.state().borrow().clone());
    }

    fn is_new_article_list(&self) -> bool {
        let imp = self.imp();
        let settings = App::default().settings();

        // Is this a new list based on changed persitent settings?
        let new_list_because_of_settings = if let Some(prev_settings) = imp.prev_settings.borrow().as_ref() {
            settings.borrow().get_article_list_order() != prev_settings.get_article_list_order()
                || settings.borrow().get_article_list_show_thumbs() != prev_settings.get_article_list_show_thumbs()
        } else {
            false
        };
        // Is this a new list based on changed window state? (e.g. different sidebar selection)
        let new_list_because_of_window_state = *self.state().borrow() != *self.prev_state().borrow();
        new_list_because_of_settings || new_list_because_of_window_state
    }

    pub fn update_article_list(&self) {
        self.update_article_list_force(false);
    }

    pub fn update_article_list_force(&self, force_new: bool) {
        let is_new_list = self.is_new_article_list() || force_new;
        let is_list_empty = self.article_list_column().article_list().get_last_row_model().is_none();
        let is_unread_list = self.state().borrow().get_article_list_mode() == &ArticleListMode::Unread;
        let article_filter = self.update_article_list_filter(is_new_list);
        let more_articles_filter = self.load_more_articles_filter();
        let mut new_list_model = ArticleListModel::new(&App::default().settings().borrow().get_article_list_order());
        let loaded_article_ids = self.article_list_column().article_list().loaded_article_ids();

        App::default().execute_with_callback(
            move |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let mut articles = news_flash.get_articles(article_filter)?;

                    let loaded_article_count = articles.len() as i64;
                    if loaded_article_count < ContentPageState::page_size() && !is_new_list && !is_list_empty {
                        // article list is not filled all the way up to "page size"
                        // load a few more
                        let mut more_articles = news_flash.get_articles(more_articles_filter)?;
                        articles.append(&mut more_articles);
                    }

                    // if this is an update of the same unread list also load the already loaded articles
                    // so they wont be removed by the update & things like the thumbnail update
                    if !is_new_list && is_unread_list {
                        if let Ok(mut a) = news_flash.get_articles(ArticleFilter::ids(loaded_article_ids)) {
                            articles.append(&mut a);
                        }
                    }

                    let (feeds, _feed_mappings) = news_flash.get_feeds()?;
                    let (tags, taggings) = news_flash.get_tags()?;
                    let articles = articles
                        .drain(..)
                        .map(|article| {
                            let feed = feeds.iter().find(|f| f.feed_id == article.feed_id);
                            let taggings: HashSet<&TagID> = taggings
                                .iter()
                                .filter(|t| t.article_id == article.article_id)
                                .map(|t| &t.tag_id)
                                .collect();
                            let tags = tags.iter().filter(|t| taggings.contains(&t.tag_id)).collect::<Vec<_>>();

                            (article, feed, tags)
                        })
                        .collect();

                    new_list_model.add(articles);

                    Ok(new_list_model)
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            clone!(@weak self as this => @default-panic, move |_app, res: Result<ArticleListModel, NewsFlashError>| {
                if let Ok(article_list_model) = res {
                    this
                        .article_list_column()
                        .article_list()
                        .update(article_list_model, &this.state(), is_new_list, &this.prev_state().borrow());
                }

                this.imp()
                    .prev_settings
                    .replace(Some(App::default().settings().borrow().clone()));
                this.prev_state().replace(this.state().borrow().clone());
            }),
        );
    }

    pub fn load_more_articles(&self) {
        let article_filter = self.load_more_articles_filter();
        let mut list_model = ArticleListModel::new(&App::default().settings().borrow().get_article_list_order());
        let article_list = self.article_list_column().article_list().clone();

        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let mut articles = news_flash.get_articles(article_filter)?;
                    let (feeds, _feed_mappings) = news_flash.get_feeds()?;
                    let (tags, taggings) = news_flash.get_tags()?;

                    let articles = articles
                        .drain(..)
                        .map(|article| {
                            let feed = feeds.iter().find(|f| f.feed_id == article.feed_id);
                            let taggings: HashSet<&TagID> = taggings
                                .iter()
                                .filter(|t| t.article_id == article.article_id)
                                .map(|t| &t.tag_id)
                                .collect();
                            let tags = tags.iter().filter(|t| taggings.contains(&t.tag_id)).collect::<Vec<_>>();

                            (article, feed, tags)
                        })
                        .collect();

                    list_model.add(articles);

                    Ok(list_model)
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |_app, res: Result<ArticleListModel, NewsFlashError>| {
                if let Ok(article_list_model) = res {
                    article_list.add_more_articles(article_list_model);
                }
            },
        );
    }

    fn should_load_unread(&self) -> Option<Read> {
        match self.state().borrow().get_article_list_mode() {
            ArticleListMode::All | ArticleListMode::Marked => None,
            ArticleListMode::Unread => Some(Read::Unread),
        }
    }

    fn should_load_marked(&self) -> Option<Marked> {
        match self.state().borrow().get_article_list_mode() {
            ArticleListMode::All | ArticleListMode::Unread => None,
            ArticleListMode::Marked => Some(Marked::Marked),
        }
    }

    fn should_load_sidebar_selection(&self) -> (Option<FeedID>, Option<CategoryID>, Option<TagID>) {
        let (selected_feed, selected_category, selected_tag) = match self.state().borrow().get_sidebar_selection() {
            SidebarSelection::All => (None, None, None),
            SidebarSelection::Tag(id, _) => (None, None, Some(id.clone())),
            SidebarSelection::FeedList(id, _title) => match id {
                FeedListItemID::Feed(mapping) => (Some(mapping.feed_id.clone()), None, None),
                FeedListItemID::Category(id) => (None, Some(id.clone()), None),
            },
        };

        (selected_feed, selected_category, selected_tag)
    }

    fn should_hide_future_articles(&self, older_than: Option<DateTime<Utc>>) -> Option<DateTime<Utc>> {
        let hide_furure_articles = App::default()
            .settings()
            .borrow()
            .get_article_list_hide_future_articles();

        if hide_furure_articles {
            if let Some(older_than) = older_than {
                if older_than < Utc::now() {
                    Some(older_than)
                } else {
                    Some(Utc::now())
                }
            } else {
                Some(Utc::now())
            }
        } else {
            older_than
        }
    }

    fn load_articles_blacklist(&self) -> (Option<Vec<FeedID>>, Option<Vec<CategoryID>>) {
        let current_undo_action = self.get_current_undo_action();
        let processing_undo_actions = self.processing_undo_actions();

        let mut undo_actions = Vec::new();
        let mut feed_blacklist = Vec::new();
        let mut category_blacklist = Vec::new();

        if let Some(current_undo_action) = current_undo_action {
            undo_actions.push(current_undo_action);
        }

        for processing_undo_action in &*processing_undo_actions.borrow() {
            undo_actions.push(processing_undo_action.clone());
        }

        for undo_action in undo_actions {
            match undo_action {
                UndoDelete::Feed(feed_id, _label) => feed_blacklist.push(feed_id.clone()),
                UndoDelete::Category(category_id, _label) => category_blacklist.push(category_id.clone()),
                UndoDelete::Tag(_tag_id, _label) => {}
            }
        }

        let feed_blacklist = if feed_blacklist.is_empty() {
            None
        } else {
            Some(feed_blacklist)
        };
        let category_blacklist = if category_blacklist.is_empty() {
            None
        } else {
            Some(category_blacklist)
        };

        (feed_blacklist, category_blacklist)
    }

    fn update_article_list_filter(&self, is_new_list: bool) -> ArticleFilter {
        let last_article_in_list_date = self
            .article_list_column()
            .article_list()
            .get_last_row_model()
            .map(|model| model.date);

        let (limit, last_article_date) = if is_new_list || last_article_in_list_date.is_none() {
            // article list is empty: load default amount of articles to fill it
            (ContentPageState::page_size(), None)
        } else {
            (i64::MAX, last_article_in_list_date)
        };

        let (older_than, newer_than) = if let Some(last_article_date) = last_article_date {
            match App::default().settings().borrow().get_article_list_order() {
                // +/- 1s to not exclude last article in list
                ArticleOrder::NewestFirst => (
                    None,
                    Some(Utc.from_utc_datetime(&last_article_date) - Duration::seconds(1)),
                ),
                ArticleOrder::OldestFirst => (
                    Some(Utc.from_utc_datetime(&last_article_date) + Duration::seconds(1)),
                    None,
                ),
            }
        } else {
            (None, None)
        };

        let search_term = self.state().borrow().get_search_term().map(String::from);
        let order = App::default().settings().borrow().get_article_list_order();

        // mutate older_than to hide articles in the future
        let older_than = self.should_hide_future_articles(older_than);

        let unread = self.should_load_unread();
        let marked = self.should_load_marked();
        let (selected_feed, selected_category, selected_tag) = self.should_load_sidebar_selection();
        let (feed_blacklist, category_blacklist) = self.load_articles_blacklist();

        ArticleFilter {
            limit: Some(limit),
            offset: None,
            order: Some(order),
            unread,
            marked,
            feeds: selected_feed.map(|f| vec![f]),
            feed_blacklist,
            categories: selected_category.map(|c| vec![c]),
            category_blacklist,
            tags: selected_tag.map(|t| vec![t]),
            ids: None,
            newer_than,
            older_than,
            search_term,
        }
    }

    fn load_more_articles_filter(&self) -> ArticleFilter {
        let search_term = self.state().borrow().get_search_term().map(String::from);
        let order = App::default().settings().borrow().get_article_list_order();

        let last_article_in_list_date = self
            .article_list_column()
            .article_list()
            .get_last_row_model()
            .map(|model| Utc.from_utc_datetime(&model.date));

        let (older_than, newer_than) = if let Some(last_article_date) = last_article_in_list_date {
            match order {
                // newest first => we want articles older than the last in list
                ArticleOrder::NewestFirst => (Some(last_article_date), None),

                // newsest first => we want article newer than the last in list
                ArticleOrder::OldestFirst => (None, Some(last_article_date)),
            }
        } else {
            (None, None)
        };

        // mutate older_than to hide articles in the future
        let older_than = self.should_hide_future_articles(older_than);

        let unread = self.should_load_unread();
        let marked = self.should_load_marked();
        let (selected_feed, selected_category, selected_tag) = self.should_load_sidebar_selection();
        let (feed_blacklist, category_blacklist) = self.load_articles_blacklist();

        ArticleFilter {
            limit: Some(ContentPageState::page_size()),
            offset: None,
            order: Some(order),
            unread,
            marked,
            feeds: selected_feed.map(|f| vec![f]),
            feed_blacklist,
            categories: selected_category.map(|c| vec![c]),
            category_blacklist,
            tags: selected_tag.map(|t| vec![t]),
            ids: None,
            newer_than,
            older_than,
            search_term,
        }
    }

    pub fn update_sidebar(&self) {
        let state = self.state().borrow().clone();
        let current_undo_action = self.get_current_undo_action();
        let processing_undo_actions = self.processing_undo_actions().borrow().clone();
        let sidebar = self.sidebar_column().sidebar().clone();

        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let mut tree = FeedListTree::new();
                    let mut tag_list_model: Option<TagListModel> = None;

                    let (categories, category_mappings) =
                        news_flash.get_categories().wrap_err("Failed to load categories")?;
                    let (feeds, mut feed_mappings) = news_flash.get_feeds().wrap_err("Failed to load feeds")?;

                    // collect unread and marked counts
                    let hide_future_articles = App::default()
                        .settings()
                        .borrow()
                        .get_article_list_hide_future_articles();
                    let feed_count_map = match state.get_article_list_mode() {
                        ArticleListMode::All | ArticleListMode::Unread => news_flash
                            .unread_count_feed_map(hide_future_articles)
                            .wrap_err("Failed to load unread counts")?,
                        ArticleListMode::Marked => news_flash
                            .marked_count_feed_map()
                            .wrap_err("Failed to load marked counts")?,
                    };

                    let mut pending_delte_feeds = HashSet::new();
                    let mut pending_delete_categories = HashSet::new();
                    let mut pending_delete_tags = HashSet::new();
                    if let Some(current_undo_action) = current_undo_action {
                        match current_undo_action {
                            UndoDelete::Feed(id, _label) => pending_delte_feeds.insert(id),
                            UndoDelete::Category(id, _label) => pending_delete_categories.insert(id),
                            UndoDelete::Tag(id, _label) => pending_delete_tags.insert(id),
                        };
                    }
                    for processing_undo_action in processing_undo_actions {
                        match processing_undo_action {
                            UndoDelete::Feed(id, _label) => pending_delte_feeds.insert(id),
                            UndoDelete::Category(id, _label) => pending_delete_categories.insert(id),
                            UndoDelete::Tag(id, _label) => pending_delete_tags.insert(id),
                        };
                    }

                    // If there are feeds without a category:
                    // - create mappings for all feeds without category to be children of the toplevel
                    let mut uncategorized_mappings =
                        Util::create_mappings_for_uncategorized_feeds(&feeds, &feed_mappings);
                    feed_mappings.append(&mut uncategorized_mappings);

                    // feedlist: Categories
                    let mut pending_categories = Vec::new();
                    for mapping in &category_mappings {
                        if pending_delete_categories.contains(&mapping.category_id) {
                            continue;
                        }

                        let category = match categories.iter().find(|c| c.category_id == mapping.category_id) {
                            Some(res) => res,
                            None => {
                                warn!(
                                    "Mapping for category '{}' exists, but can't find the category itself",
                                    mapping.category_id
                                );
                                App::default().in_app_notifiaction(&i18n_f(
                                    "Sidebar: missing category with id: '{}'",
                                    &[&mapping.category_id.to_string()],
                                ));
                                continue;
                            }
                        };

                        let category_item_count = Util::calculate_item_count_for_category(
                            &category.category_id,
                            &feed_mappings,
                            &category_mappings,
                            &feed_count_map,
                            &pending_delte_feeds,
                            &pending_delete_categories,
                        );

                        pending_categories.push((category, mapping, category_item_count));
                    }
                    tree.add_categories(&pending_categories);

                    // feedlist: Feeds
                    for mapping in &feed_mappings {
                        if pending_delte_feeds.contains(&mapping.feed_id)
                            || pending_delete_categories.contains(&mapping.category_id)
                        {
                            continue;
                        }

                        let feed = match feeds.iter().find(|feed| feed.feed_id == mapping.feed_id) {
                            Some(res) => res,
                            None => {
                                warn!(
                                    "Mapping for feed '{}' exists, but can't find the feed itself",
                                    mapping.feed_id
                                );
                                App::default().in_app_notifiaction(&i18n_f(
                                    "Sidebar: missing feed with id: '{}'",
                                    &[&mapping.feed_id.to_string()],
                                ));
                                continue;
                            }
                        };

                        let item_count = match feed_count_map.get(&mapping.feed_id) {
                            Some(count) => *count,
                            None => 0,
                        };
                        if tree.add_feed(feed, mapping, item_count).is_err() {}
                    }

                    // tag list
                    let support_tags = App::default().features().contains(PluginCapabilities::SUPPORT_TAGS);

                    if support_tags {
                        let mut list = TagListModel::new();
                        let (tags, _taggings) = news_flash.get_tags().wrap_err("Failed to load tags")?;

                        if !tags.is_empty() {
                            for tag in tags {
                                if pending_delete_tags.contains(&tag.tag_id) {
                                    continue;
                                }
                                list.add(&tag).wrap_err("Failed to add tag to list")?;
                            }
                        }

                        tag_list_model = Some(list);
                    }

                    //let total_item_count = feed_count_map.iter().map(|(_key, value)| value).sum();
                    let total_item_count = Util::calculate_item_count_for_category(
                        &NEWSFLASH_TOPLEVEL,
                        &feed_mappings,
                        &category_mappings,
                        &feed_count_map,
                        &pending_delte_feeds,
                        &pending_delete_categories,
                    );

                    Ok((total_item_count, tree, tag_list_model))
                } else {
                    Err(eyre::Report::msg("not logged in"))
                }
            },
            move |_app, res: Result<_, eyre::Report>| match res {
                Ok((total_count, feed_list_model, tag_list_model)) => {
                    sidebar.update_feedlist(feed_list_model);
                    sidebar.update_all(total_count);
                    if let Some(tag_list_model) = tag_list_model {
                        if tag_list_model.is_empty() {
                            sidebar.hide_taglist();
                        } else {
                            sidebar.update_taglist(tag_list_model);
                            sidebar.show_taglist();
                        }
                    } else {
                        sidebar.hide_taglist();
                    }
                }
                Err(error) => {
                    App::default().in_app_notifiaction(&i18n_f("Failed to update sidebar: '{}'", &[&error.to_string()]))
                }
            },
        );
    }

    pub fn article_view_scroll_diff(&self, diff: f64) {
        self.articleview_column().article_view().animate_scroll_diff(diff)
    }

    pub fn sidebar_select_next_item(&self) {
        self.sidebar_column().sidebar().select_next_item()
    }

    pub fn sidebar_select_prev_item(&self) {
        self.sidebar_column().sidebar().select_prev_item()
    }

    pub fn set_offline(&self, offline: bool) {
        self.state().borrow_mut().set_offline(offline);
        self.articleview_column().set_offline(offline);
        self.article_list_column().set_offline(offline);
        self.sidebar_column().set_offline(offline);
    }
}
