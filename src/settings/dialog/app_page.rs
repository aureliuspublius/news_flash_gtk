use crate::app::{IMAGE_DATA_DIR, WEBKIT_DATA_DIR};
use crate::i18n::i18n;
use crate::settings::UserDataSize;
use crate::util::Util;
use crate::{
    app::App,
    settings::general::{KeepArticlesDuration, PredefinedSyncInterval, SyncInterval},
};
use chrono::Duration;
use futures::{channel::oneshot, FutureExt};
use glib::{clone, Propagation, RustClosure};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{Button, CheckButton, ClosureExpression, CompositeTemplate, Entry, Label, Switch, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, ComboRow, EnumListItem, EnumListModel, PreferencesPage};
use news_flash::error::NewsFlashError;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/settings/app.blp")]
    pub struct SettingsAppPage {
        #[template_child]
        pub keep_running_switch: TemplateChild<Switch>,
        #[template_child]
        pub autostart_switch: TemplateChild<Switch>,
        #[template_child]
        pub startup_sync_switch: TemplateChild<Switch>,
        #[template_child]
        pub metered_sync_switch: TemplateChild<Switch>,
        #[template_child]
        pub predefined_sync_row: TemplateChild<ComboRow>,
        #[template_child]
        pub custom_update_entry: TemplateChild<Entry>,
        #[template_child]
        pub manual_update_check: TemplateChild<CheckButton>,
        #[template_child]
        pub predefined_update_check: TemplateChild<CheckButton>,
        #[template_child]
        pub custom_update_check: TemplateChild<CheckButton>,
        #[template_child]
        pub clear_cache_button: TemplateChild<Button>,
        #[template_child]
        pub user_data_label: TemplateChild<Label>,
        #[template_child]
        pub cache_label: TemplateChild<Label>,
        #[template_child]
        pub limit_articles_row: TemplateChild<ComboRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsAppPage {
        const NAME: &'static str = "SettingsAppPage";
        type ParentType = PreferencesPage;
        type Type = super::SettingsAppPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsAppPage {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for SettingsAppPage {}

    impl PreferencesPageImpl for SettingsAppPage {}
}

glib::wrapper! {
    pub struct SettingsAppPage(ObjectSubclass<imp::SettingsAppPage>)
        @extends Widget, PreferencesPage;
}

impl Default for SettingsAppPage {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SettingsAppPage {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let settings = App::default().settings();
        let imp = self.imp();

        imp.keep_running_switch
            .set_active(settings.borrow().get_keep_running_in_background());
        imp.autostart_switch
            .set_active(settings.borrow().get_autostart() && settings.borrow().get_keep_running_in_background());
        imp.autostart_switch
            .set_sensitive(settings.borrow().get_keep_running_in_background());

        imp.startup_sync_switch
            .set_active(settings.borrow().get_sync_on_startup());
        imp.metered_sync_switch
            .set_active(settings.borrow().get_sync_on_metered());

        match settings.borrow().get_sync_interval() {
            SyncInterval::Never => {
                imp.manual_update_check.set_active(true);
                imp.custom_update_entry.set_sensitive(false);
            }
            SyncInterval::Predefined(_) => {
                imp.predefined_update_check.set_active(true);
                imp.custom_update_entry.set_sensitive(false);
            }
            SyncInterval::Custom(total_seconds) => {
                let hours = total_seconds / 3600;
                let minutes = (total_seconds - hours * 3600) / 60;
                let seconds = total_seconds - hours * 3600 - minutes * 60;

                imp.custom_update_entry
                    .set_text(&format!("{:02}:{:02}:{:02}", hours, minutes, seconds));
                imp.custom_update_check.set_active(true);
                imp.custom_update_entry.set_sensitive(true);
            }
        }

        let params: &[gtk4::Expression] = &[];
        let closure = RustClosure::new(|values| {
            let e = values[0].get::<EnumListItem>().unwrap();
            let sync = PredefinedSyncInterval::from_u32(e.value() as u32)
                .to_string()
                .to_value();
            Some(sync)
        });
        let sync_interval_closure = ClosureExpression::new::<String>(params, closure);
        imp.predefined_sync_row.set_expression(Some(&sync_interval_closure));
        imp.predefined_sync_row
            .set_model(Some(&EnumListModel::new(PredefinedSyncInterval::static_type())));
        imp.predefined_sync_row.set_selected(
            if let SyncInterval::Predefined(predefined_interval) = settings.borrow().get_sync_interval() {
                predefined_interval.as_u32()
            } else {
                0
            },
        );

        let closure = RustClosure::new(|values| {
            let e = values[0].get::<EnumListItem>().unwrap();
            let keep = KeepArticlesDuration::from_u32(e.value() as u32).to_string().to_value();
            Some(keep)
        });
        let limit_articles_closure = ClosureExpression::new::<String>(params, closure);
        imp.limit_articles_row.set_expression(Some(&limit_articles_closure));
        imp.limit_articles_row
            .set_model(Some(&EnumListModel::new(KeepArticlesDuration::static_type())));

        let limit_articles_row = imp.limit_articles_row.clone();
        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.get_keep_articles_duration().await
                } else {
                    None
                }
            },
            move |_app, keep_articles_duration| {
                limit_articles_row.set_selected(KeepArticlesDuration::from_duration(keep_articles_duration).as_u32());
            },
        );

        self.setup_data_section();
        self.setup_interaction();
    }

    fn setup_interaction(&self) {
        let imp = self.imp();

        imp.keep_running_switch.connect_state_set(
            clone!(@weak self as this => @default-panic, move |_switch, is_set| {
                let imp = this.imp();

                if is_set {
                    App::request_background_permission(App::default().settings().borrow().get_autostart());
                } else {
                    imp.autostart_switch.set_state(false);
                    _ = App::default().settings().borrow_mut().set_autostart(is_set);
                }

                imp.autostart_switch.set_sensitive(is_set);

                if App::default()
                    .settings()
                    .borrow_mut()
                    .set_keep_running_in_background(is_set)
                    .is_err()
                {
                    App::default().in_app_notifiaction(&i18n("Failed to set setting 'keep running'"));
                }
                Propagation::Proceed
            }),
        );

        imp.autostart_switch.connect_state_set(|_switch, is_set| {
            App::request_background_permission(is_set);

            if App::default().settings().borrow_mut().set_autostart(is_set).is_err() {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'keep running'"));
            }
            Propagation::Proceed
        });

        imp.startup_sync_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .borrow_mut()
                .set_sync_on_startup(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'sync on startup'"));
            }
            Propagation::Proceed
        });

        imp.metered_sync_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .borrow_mut()
                .set_sync_on_metered(is_set)
                .is_err()
            {
                App::default().in_app_notifiaction(&i18n("Failed to set setting 'sync on metered'"));
            }
            Propagation::Proceed
        });

        imp.manual_update_check
            .connect_toggled(clone!(@weak self as this => @default-panic, move |check| {
                if check.is_active() {
                    let imp = this.imp();
                    imp.custom_update_entry.set_sensitive(false);
                    Self::set_sync_interval(SyncInterval::Never);
                }
            }));

        imp.predefined_update_check.connect_toggled(clone!(@weak self as this => @default-panic, move |check| {
            if check.is_active() {
                let imp = this.imp();
                imp.custom_update_entry.set_sensitive(false);
                let sync_interval = SyncInterval::Predefined(PredefinedSyncInterval::from_u32(imp.predefined_sync_row.selected()));
                Self::set_sync_interval(sync_interval);
            }
        }));

        imp.custom_update_check
            .connect_toggled(clone!(@weak self as this => @default-panic, move |check| {
                if check.is_active() {
                    let imp = this.imp();
                    imp.custom_update_entry.set_sensitive(true);
                    Self::parse_custom_sync_interval(&imp.custom_update_entry.get());
                }
            }));

        imp.predefined_sync_row
            .connect_selected_notify(clone!(@weak self as this => @default-panic, move |row| {
                let imp = this.imp();
                if imp.predefined_update_check.is_active() {
                    let sync_interval = SyncInterval::Predefined(PredefinedSyncInterval::from_u32(row.selected()));
                    Self::set_sync_interval(sync_interval);
                }
            }));

        imp.custom_update_entry
            .connect_changed(clone!(@weak self as this => @default-panic, move |entry| {
                let imp = this.imp();

                if imp.custom_update_check.is_active() {
                    Self::parse_custom_sync_interval(entry);
                }
            }));

        imp.limit_articles_row.connect_selected_notify(|row| {
            let limit_articles_duration = KeepArticlesDuration::from_u32(row.selected());
            App::default().execute_with_callback(
                move |news_flash, _client| async move {
                    if let Some(news_flash) = news_flash.read().await.as_ref() {
                        news_flash
                            .set_keep_articles_duration(limit_articles_duration.as_duration())
                            .await
                    } else {
                        Err(NewsFlashError::NotLoggedIn)
                    }
                },
                |_app, res| {
                    if res.is_err() {
                        App::default().in_app_notifiaction(&i18n("Failed to set setting 'limit articles duration'"));
                    }
                },
            );
        });

        let user_data_label = imp.user_data_label.get();
        let cache_label = imp.cache_label.get();
        imp.clear_cache_button.connect_clicked(clone!(@weak user_data_label,
            @weak cache_label => @default-panic, move |button| {

            button.set_sensitive(false);
            let (oneshot_sender, receiver) = oneshot::channel::<()>();


            App::default().execute(|news_flash, _clinet| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    _ = news_flash.delete_all_images();
                }
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .article_view()
                    .clear_cache(oneshot_sender);
            });

            let glib_future = receiver.map(clone!(
                @weak user_data_label,
                @weak cache_label,
                @weak button => @default-panic, move |res| if let Ok(()) = res {
                    Self::query_data_sizes(&user_data_label, &cache_label);
                    button.set_sensitive(true);
            }));
            Util::glib_spawn_future(glib_future);
        }));
    }

    fn setup_data_section(&self) {
        let imp = self.imp();
        Self::query_data_sizes(&imp.user_data_label, &imp.cache_label);
    }

    fn query_data_sizes(user_data_label: &Label, cache_label: &Label) {
        let user_data_label = user_data_label.clone();
        let cache_label = cache_label.clone();

        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let webkit_size = news_flash::util::folder_size(&WEBKIT_DATA_DIR).unwrap_or(0);
                    let pictures_size = news_flash::util::folder_size(&IMAGE_DATA_DIR).unwrap_or(0);
                    let db_size = news_flash.database_size().ok();

                    db_size.map(|db_size| UserDataSize {
                        database: db_size,
                        webkit: webkit_size,
                        pictures: pictures_size,
                    })
                } else {
                    None
                }
            },
            move |_app, db_size| {
                if let Some(user_data_size) = db_size {
                    user_data_label.set_text(&Util::format_data_size(user_data_size.database.on_disk));
                    cache_label.set_text(&Util::format_data_size(user_data_size.webkit + user_data_size.pictures));
                }
            },
        );
    }

    fn parse_custom_sync_interval(entry: &Entry) {
        let text = entry.text();
        let text = text.as_str();
        let char_count = text.chars().count();

        if char_count == 0 {
            entry.remove_css_class("error");
            return;
        } else if char_count != 8 {
            entry.add_css_class("error");
            return;
        }

        let text_pieces: Vec<&str> = text.split(':').collect();
        if text_pieces.len() != 3 {
            entry.add_css_class("error");
            return;
        }

        let hours = text_pieces[0].parse::<u32>().ok();
        let minutes = text_pieces[1].parse::<u32>().ok();
        let seconds = text_pieces[2].parse::<u32>().ok();

        if hours.is_none() || minutes.is_none() || seconds.is_none() {
            entry.add_css_class("error");
            return;
        }

        let hours = hours.unwrap();
        let minutes = minutes.unwrap();
        let seconds = seconds.unwrap();

        let duration =
            Duration::hours(hours as i64) + Duration::minutes(minutes as i64) + Duration::seconds(seconds as i64);
        let total_seconds = duration.num_seconds() as u32;

        if total_seconds == 0 {
            entry.add_css_class("error");
            return;
        }

        entry.remove_css_class("error");
        Self::set_sync_interval(SyncInterval::Custom(total_seconds));
    }

    fn set_sync_interval(sync_interval: SyncInterval) {
        if App::default()
            .settings()
            .borrow_mut()
            .set_sync_interval(sync_interval)
            .is_ok()
        {
            App::default().schedule_sync();
        } else {
            App::default().in_app_notifiaction(&i18n("Failed to set setting 'sync interval'"));
        }
    }
}
