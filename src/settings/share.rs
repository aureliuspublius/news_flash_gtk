use serde::{Deserialize, Serialize};
use std::default::Default;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ShareSettings {
    #[serde(default)]
    pub pocket_enabled: bool,
    #[serde(default)]
    pub instapaper_enabled: bool,
    #[serde(default)]
    pub twitter_enabled: bool,
    #[serde(default)]
    pub mastodon_enabled: bool,
    #[serde(default)]
    pub reddit_enabled: bool,
    #[serde(default)]
    pub telegram_enabled: bool,

    #[serde(default)]
    pub custom_enabled: bool,
    #[serde(default)]
    pub custom_name: Option<String>,
    #[serde(default)]
    pub custom_url: Option<String>,
}

impl Default for ShareSettings {
    fn default() -> Self {
        Self {
            pocket_enabled: true,
            instapaper_enabled: true,
            twitter_enabled: true,
            mastodon_enabled: true,
            reddit_enabled: true,
            telegram_enabled: true,

            custom_enabled: false,
            custom_name: None,
            custom_url: None,
        }
    }
}
