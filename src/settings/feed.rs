use serde::{Deserialize, Serialize};

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct FeedSettings {
    pub inline_math: Option<String>,
}
