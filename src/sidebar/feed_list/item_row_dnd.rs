use futures::channel::oneshot;
use futures_util::FutureExt;
use glib::subclass;
use gtk4::{subclass::prelude::*, traits::WidgetExt, Box, CompositeTemplate, Image, Label, Widget};
use log::warn;
use news_flash::models::FavIcon;
use std::str;

use crate::{app::App, sidebar::FeedListItemID, util::Util};

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/feedlist_dnd_icon.blp")]
    #[derive(Default)]
    pub struct ItemRowDnD {
        #[template_child]
        pub item_count_label: TemplateChild<Label>,
        #[template_child]
        pub title: TemplateChild<Label>,
        #[template_child]
        pub favicon: TemplateChild<Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ItemRowDnD {
        const NAME: &'static str = "ItemRowDnD";
        type ParentType = Box;
        type Type = super::ItemRowDnD;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ItemRowDnD {}

    impl WidgetImpl for ItemRowDnD {}

    impl BoxImpl for ItemRowDnD {}
}

glib::wrapper! {
    pub struct ItemRowDnD(ObjectSubclass<imp::ItemRowDnD>)
        @extends Widget, Box;
}

impl Default for ItemRowDnD {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ItemRowDnD {
    pub fn new(id: FeedListItemID, title: &str, count: u32) -> Self {
        let widget = Self::default();
        widget.update_favicon(Some(id));
        widget.update_title(title);
        widget.update_item_count(count);
        widget
    }

    fn update_title(&self, title: &str) {
        let imp = self.imp();
        imp.title.set_text(title);
    }

    fn update_item_count(&self, count: u32) {
        let imp = self.imp();

        if count > 0 {
            imp.item_count_label.set_label(&count.to_string());
            imp.item_count_label.set_visible(true);
        } else {
            imp.item_count_label.set_visible(false);
        }
    }

    fn update_favicon(&self, feed_id: Option<FeedListItemID>) {
        let imp = self.imp();

        if let Some(FeedListItemID::Feed(feed_mapping)) = feed_id {
            let (sender, receiver) = oneshot::channel::<Option<FavIcon>>();
            App::default().load_favicon(feed_mapping.feed_id.clone(), sender);

            let this = self.clone();
            let glib_future = receiver.map(move |res| match res {
                Ok(Some(icon)) => {
                    if let Some(data) = icon.data {
                        let bytes = glib::Bytes::from_owned(data);
                        let texture = gdk4::Texture::from_bytes(&bytes);
                        if let Ok(texture) = texture {
                            let imp = this.imp();
                            imp.favicon.set_visible(true);
                            imp.favicon.set_from_paintable(Some(&texture));
                        } else if let Err(error) = texture {
                            log::debug!("Favicon '{}': {}", icon.feed_id, error);
                        }
                    }
                }
                Ok(None) => {
                    warn!("Favicon does not contain image data.");
                }
                Err(_) => warn!("Receiving favicon failed."),
            });

            Util::glib_spawn_future(glib_future);
        } else {
            imp.favicon.set_visible(false);
        }
    }
}
